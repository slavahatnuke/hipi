const HI_FUNCTION = Symbol('HI_FUNCTION');
const HI_UNDEFINED = Symbol('UNDEFINED');
const HI_SUBJECT = Symbol('HI_SUBJECT');

function isFunction(fn) {
  return fn instanceof Function;
}

function fun(fn, errorMessage = null) {
  errorMessage = errorMessage || 'Expects function';

  if (isFunction(fn)) {
    return fn;
  }
  throw new Error(errorMessage);
}

function isUndefined(value) {
  return HI_UNDEFINED === value;
}

function isHiFunction(fn) {
  return fn instanceof Function && !!fn[HI_FUNCTION];
}

module.exports = {
  isFunction,
  fun,
  isUndefined,
  isHiFunction,

  HI_FUNCTION,
  HI_UNDEFINED,
  HI_SUBJECT
};
