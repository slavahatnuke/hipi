const {
  fun,
  isUndefined,
  isHiFunction,
  isFunction,

  HI_FUNCTION,
  HI_UNDEFINED,
  HI_SUBJECT
} = require('./common');


function isSubject(value) {
  return isFunction(value) && !!value[HI_SUBJECT];
}

function Subject(input = undefined, { transformer = null } = {}) {
  if (isHiFunction(input)) {
    const { subject } = input[HI_FUNCTION];
    return subject;
  }

  if (isSubject(input)) {
    return input;
  }

  const subjectContext = {
    value: input
  };

  transformer = transformer || (value => value);
  transformer = fun(transformer);

  let subscribers = [];

  function subject(value) {
    subscribers.map(aSubscriber => aSubscriber(value));

    value = transformer(value);
    subjectContext.value = value;

    return value;
  }

  function unsubscribe(subscriber) {
    subscribers = subscribers.filter(aSubscriber => aSubscriber !== subscriber);
  }

  subject.subscribe = function subscribe(subscriber) {
    subscriber = fun(subscriber, 'Subscriber is not a function');
    subscribers.push(subscriber);

    return function subjectUnsubscribe() {
      unsubscribe(subscriber);
    };
  };

  subject.unsubscribe = unsubscribe;
  subject.value = () => subjectContext.value;

  subject.destroy = function destroy() {
    subscribers = [];
    subjectContext.value = undefined;
  };

  subject[HI_SUBJECT] = subjectContext;

  return subject;
}

function Hi(funConstructor) {
  const factory = function hiFactory() {
    const subject = Subject();
    const valueFunction = fun(funConstructor(subject));

    valueFunction[HI_FUNCTION] = {
      factory,
      subject
    };

    return valueFunction;
  };

  return factory();
}

function hi(fn) {
  fn = fun(fn);

  return Hi(subject => function hi(value, outputNext = null) {
    let response = HI_UNDEFINED;

    outputNext = outputNext ? fun(outputNext) : result => response = result;

    fn(value, (result) => {
      response = result;
      subject(result);
      outputNext(result);
    });

    return response;
  });
}

function hiFun(fn, { errorMessage = null } = {}) {
  fn = fun(fn, errorMessage);

  if (isHiFunction(fn)) {
    return fn;
  }
  return hi((value, next) => next(fn(value)));
}

function piFun(...funs) {
  funs = funs.map(hiFun);

  const piFunctions = funs.map((fn, idx) => {
    const nextIdx = idx + 1;

    return function piFunction(value, outputNext = null) {
      const fnNext = piFunctions[nextIdx] || outputNext;

      return fn(value, value => fnNext(value, outputNext));
    };
  });

  const [first] = piFunctions;

  return first;
}

function pi(...funs) {
  if (!funs.length) {
    return hiFun(value => value);
  }

  return hi(piFun(...funs));
}


// primitives
function Source(fn) {
  fn = fun(fn);
  return hi((value, next) => {
    const creator = fn(next);

    if (isFunction(creator)) {
      creator(value);
    }
  });
}

function ArraySource(inputArray = []) {
  function publishArray(array, next) {
    array = Array
      .from(array)
      .reverse();

    while (array.length) {
      next(array.pop());
    }
  }

  return Source((next) => {
    publishArray(inputArray, next);
    inputArray = [];

    return (value) => {
      if (Array.isArray(value)) {
        publishArray(value, next);
      }
    };
  });
}

function Filter(fn) {
  fn = fun(fn);
  return hi((value, next) => (fn(value) ? next(value) : null));
}

module.exports = {
  isUndefined,
  isHiFunction,

  hi,
  pi,

  Subject,
  isSubject,

  Source,
  ArraySource,
  Filter
};
