const { expect } = require('chai');

const {
  describe, it
} = require('mocha');

const {
  hi, isUndefined, isHiFunction,

  pi,
  Source,
  ArraySource,
  Filter,

  Subject,
  isSubject
} = require('../sync');

describe('hi sync', () => {
  it('hi', () => {
    const fn1 = hi((value, next) => next(value));
    expect(fn1('any-input')).equal('any-input');

    const fn2 = hi(value => value);

    expect(isHiFunction(fn2)).equal(true);
    expect(isUndefined(fn2('any-input'))).equal(true);
  });

  it('pi with hi functions', () => {
    const addFive = pi(
      hi((value, next) => next(value + 1)),
      hi((value, next) => next(value + 1)),
      hi((value, next) => next(value + 3))
    );

    expect(addFive(0)).equal(5);
  });

  it('pi with pure function', () => {
    const addFive = pi(
      value => value,
      value => value + 1,
      value => value + 4
    );

    expect(addFive(0)).equal(5);
  });

  it('pi reactive inside', () => {
    let counterIn = 0;
    let counterOut = 0;

    const outputs = [];

    const addValues3Times = pi(
      hi((value, next) => {
        counterIn += 1;
        // next(value + 120)
        next(value + 133);
        next(value + 120);
        next(value + 1);
      }),
      hi((value, next) => next(value)),
      hi((value, next) => {
        outputs.push(value);

        counterOut += 1;
        next(value);
      })
    );

    expect(addValues3Times(1)).equal(2);

    expect(counterIn).equal(1);
    expect(counterOut).equal(3);

    expect(outputs).deep.equal([134, 121, 2]);
  });


  it('errors', () => {
    const catchError = pi(
      value => value + 1,
      () => {
        throw new Error('Woo');
      }
    );

    expect(() => catchError(0)).to.throw('Woo');
  });


  it('pi & hi throws error if not a function', () => {
    expect(() => hi('woo')).to.throw('Expects function');
    expect(() => hi('pi')).to.throw('Expects function');
  });

  it('source', () => {
    const source = Source((next) => {
      next(1);
      next(2);
      next(3);
    });

    const outputs = [];

    const pipe1 = pi(
      source,
      value => outputs.push(value)
    );

    expect(outputs).deep.equal([]);

    pipe1();

    expect(outputs).deep.equal([1, 2, 3]);
  });

  it('array source', () => {
    const source = ArraySource([
      12,
      34,
      56,
      78
    ]);

    const outputs = [];

    const pipe1 = pi(
      source,
      value => outputs.push(value)
    );

    expect(outputs).deep.equal([]);

    pipe1();

    expect(outputs).deep.equal([
      12,
      34,
      56,
      78
    ]);

    pipe1([125, 126]);

    expect(outputs).deep.equal([
      12,
      34,
      56,
      78,
      125,
      126
    ]);
  });

  it('filter', () => {
    const source = ArraySource([
      1,
      2,
      3,
      4,
      5,
      6,
      7
    ]);

    const outputs = [];

    const pipe1 = pi(
      source,
      Filter(value => value > 3),
      value => outputs.push(value)
    );

    expect(outputs).deep.equal([]);

    pipe1();

    expect(outputs).deep.equal([
      4,
      5,
      6,
      7
    ]);
  });

  it('Subject / subscribe / unsubscribe', () => {
    const subject = Subject();
    const output = [];
    const unsubscribe = subject.subscribe(value => output.push(value));

    expect(isSubject(subject)).deep.equal(true);
    expect(isSubject(123)).deep.equal(false);

    expect(output).deep.equal([]);

    expect(subject(22)).equal(22);

    subject(33);
    subject(5511);

    expect(output).deep.equal([22, 33, 5511]);

    unsubscribe();

    subject(222222);

    expect(output).deep.equal([22, 33, 5511]);
  });


  it('Subject / unsubscribe method', () => {
    const subject = Subject();
    const output = [];

    const subscriber = value => output.push(value);
    subject.subscribe(subscriber);

    expect(output).deep.equal([]);

    subject(22);
    subject(33);
    subject(5511);

    expect(output).deep.equal([22, 33, 5511]);

    subject.unsubscribe(subscriber);

    subject(222222);

    expect(output).deep.equal([22, 33, 5511]);
  });

  it('Subject / subscribe / destroy', () => {
    const subject = Subject();
    const output = [];

    subject.subscribe(value => output.push(value));

    expect(output).deep.equal([]);

    subject(123);
    subject(456);

    expect(output).deep.equal([123, 456]);

    subject.destroy();

    subject(555777);

    expect(output).deep.equal([123, 456]);
  });

  it('Subject / Subject of hi function', () => {
    const hi1 = hi((value, next) => next(value));
    const subject = Subject(hi1);

    let expected = null;
    subject.subscribe(value => expected = value);

    hi1('okk1');

    expect(expected).equal('okk1');
  });


  it('Subject returns subject', () => {
    const subject1 = Subject();
    const subject2 = Subject(subject1);

    expect(subject2).equal(subject1);
  });

  it('Subject transforms data', () => {
    const subject = Subject(null, { transformer: value => value + 1 });
    expect(subject(1)).equal(2);
  });

  it('Subject available last value', () => {
    const subject = Subject();
    expect(subject.value()).equal(undefined);
    subject('abcd');
    expect(subject.value()).equal('abcd');
  });

  it('Subject of value', () => {
    const subject = Subject(1234);
    expect(subject.value()).equal(1234);
    subject('abcd');
    expect(subject.value()).equal('abcd');
  });

  it('pi without functions', () => {
    const nothingToDo = pi();

    expect(nothingToDo(3)).equal(3);
  });
});
