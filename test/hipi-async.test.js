const { expect } = require('chai');

const {
  describe, it
} = require('mocha');

const {
  hi, isUndefined, isHiFunction,

  pi,
  Source,
  ArraySource,
  Filter,

  Subject,
  isSubject
} = require('..');

describe('hi async', () => {
  it('hi', async () => {
    const fn1 = hi(async (value, next) => next(value));
    expect(await fn1('any-input')).equal('any-input');

    const fn2 = hi(async value => value);

    expect(isHiFunction(fn2)).equal(true);
    expect(isUndefined(await fn2('any-input'))).equal(true);
  });

  it('pi with hi functions', async () => {
    const addFive = pi(
      hi(async (value, next) => next(value + 1)),
      hi((value, next) => next(value + 1)),
      hi((value, next) => next(value + 3))
    );

    expect(await addFive(0)).equal(5);
  });

  it('pi with pure function', async () => {
    const addFive = pi(
      value => value,
      value => value + 1,
      value => value + 4
    );

    expect(await addFive(0)).equal(5);
  });

  it('pi reactive inside', async () => {
    let counterIn = 0;
    let counterOut = 0;

    const outputs = [];

    const addValues3Times = pi(
      hi(async (value, next) => {
        counterIn += 1;
        // next(value + 120)
        await next(value + 133);
        await next(value + 120);
        await next(value + 1);
      }),
      hi(async (value, next) => next(value)),
      hi(async (value, next) => {
        outputs.push(value);

        counterOut += 1;
        await next(value);
      })
    );

    expect(await addValues3Times(1)).equal(2);

    expect(counterIn).equal(1);
    expect(counterOut).equal(3);

    expect(outputs).deep.equal([134, 121, 2]);
  });


  it('errors', async () => {
    const catchError = pi(
      async value => value + 1,
      async () => {
        throw new Error('Woo');
      }
    );

    let error;
    try {
      await catchError(0);
    } catch (e) {
      error = e;
    }

    expect(error.message).equal('Woo');
  });

  it('pi & hi throws error if not a function', async () => {
    let error;

    error = null;

    try {
      await hi('woo');
    } catch (e) {
      error = e;
    }

    expect(error.message).equal('Expects function');

    error = null;

    try {
      await pi('woo');
    } catch (e) {
      error = e;
    }

    expect(error.message).equal('Expects function');
  });

  it('source', async () => {
    const source = Source(async (next) => {
      await next(1);
      await next(2);
      await next(3);
    });

    const outputs = [];

    const pipe1 = pi(
      source,
      value => outputs.push(value)
    );

    expect(outputs).deep.equal([]);

    await pipe1();

    expect(outputs).deep.equal([1, 2, 3]);
  });

  it('array source', async () => {
    const source = ArraySource([
      12,
      34,
      56,
      78
    ]);

    const outputs = [];

    const pipe1 = pi(
      source,
      async value => outputs.push(value)
    );

    expect(outputs).deep.equal([]);

    await pipe1();

    expect(outputs).deep.equal([
      12,
      34,
      56,
      78
    ]);

    await pipe1([125, 126]);

    expect(outputs).deep.equal([
      12,
      34,
      56,
      78,
      125,
      126
    ]);
  });

  it('filter', async () => {
    const source = ArraySource([
      1,
      2,
      3,
      4,
      5,
      6,
      7
    ]);

    const outputs = [];

    const pipe1 = pi(
      source,
      Filter(async value => value > 3),
      value => outputs.push(value)
    );

    expect(outputs).deep.equal([]);

    await pipe1();

    expect(outputs).deep.equal([
      4,
      5,
      6,
      7
    ]);
  });

  it('Subject / subscribe / unsubscribe', async () => {
    const subject = Subject();
    const output = [];
    const unsubscribe = subject.subscribe(async value => output.push(value));

    expect(isSubject(subject)).deep.equal(true);
    expect(isSubject(123)).deep.equal(false);

    expect(output).deep.equal([]);

    expect(await subject(22)).equal(22);

    await subject(33);
    await subject(5511);

    expect(output).deep.equal([22, 33, 5511]);

    unsubscribe();

    await subject(222222);

    expect(output).deep.equal([22, 33, 5511]);
  });


  it('Subject / unsubscribe method', async () => {
    const subject = Subject();
    const output = [];

    const subscriber = async value => output.push(value);
    subject.subscribe(subscriber);

    expect(output).deep.equal([]);

    await subject(22);
    await subject(33);
    await subject(5511);

    expect(output).deep.equal([22, 33, 5511]);

    subject.unsubscribe(subscriber);

    await subject(222222);

    expect(output).deep.equal([22, 33, 5511]);
  });

  it('Subject / subscribe / destroy', async () => {
    const subject = Subject();
    const output = [];

    subject.subscribe(value => output.push(value));

    expect(output).deep.equal([]);

    await subject(123);
    await subject(456);

    expect(output).deep.equal([123, 456]);

    subject.destroy();

    await subject(555777);

    expect(output).deep.equal([123, 456]);
  });


  it('Subject / Subject of hi function', async () => {
    const hi1 = hi(async (value, next) => next(value));
    const subject = Subject(hi1);

    let expected = null;
    subject.subscribe(value => expected = value);

    await hi1('okk1');

    expect(expected).equal('okk1');
  });

  it('Subject returns subject', async () => {
    const subject1 = Subject();
    const subject2 = Subject(subject1);

    expect(subject2).equal(subject1);
  });

  it('Subject transforms data', async () => {
    const subject = Subject(null, { transformer: async value => value + 1 });
    expect(await subject(1)).equal(2);
  });

  it('Subject available last value', async () => {
    const subject = Subject();
    expect(subject.value()).equal(undefined);
    await subject('abcd');
    expect(subject.value()).equal('abcd');
  });


  it('Subject of value', async () => {
    const subject = Subject(1234);
    expect(subject.value()).equal(1234);
    await subject('abcd');
    expect(subject.value()).equal('abcd');
  });

  it('pi without functions', async () => {
    const nothingToDo = pi();

    expect(await nothingToDo(3)).equal(3);
  });

  it('add five concurrent', async () => {
    const addFive = pi(
      async value => value + 1,
      async value => value + 4
    );

    expect(await addFive(3)).equal(8);

    const [first, last] = await Promise.all([5, 10].map(v => addFive(v)));

    expect(first).equal(10);
    expect(last).equal(15);
  });
});
