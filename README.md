# hipi
`hipi` is functional reactive pipes & functions `async & sync`
- async is default `require('hipi')`
- sync is `require('hipi/sync')`

```javascript
    const {hi, pi, Subject} = require('hipi');
```

### hi

`hi` - is `reactive` function
```javascript
    const fn1 = hi(async (value, next) => next(value));
    expect(await fn1('any-input')).equal('any-input');
```

### pi

`pi` - is `pipe` with hi functions.
```javascript
    const addFive = pi(
      hi(async (value, next) => next(value + 1)),
      hi((value, next) => next(value + 1)),
      hi((value, next) => next(value + 3))
    );

    expect(await addFive(0)).equal(5);
```

`pi` - is `pipe` with pure functions.
```javascript
    const addFive = pi(
      value => value,
      value => value + 1,
      value => value + 4
    );

    expect(await addFive(0)).equal(5);
```

`pi` - is `reactive`. Every call of next will be processed.
```javascript
    let counterIn = 0;
    let counterOut = 0;

    const outputs = [];

    const addValues3Times = pi(
      hi(async (value, next) => {
        counterIn += 1;
        await next(value + 133);
        await next(value + 120);
        await next(value + 1);
      }),
      hi(async (value, next) => next(value)),
      hi(async (value, next) => {
        outputs.push(value);

        counterOut += 1;
        await next(value);
      })
    );

    expect(await addValues3Times(1)).equal(2);

    expect(counterIn).equal(1);
    expect(counterOut).equal(3);

    expect(outputs).deep.equal([134, 121, 2]);
```

### Subject
```javascript
    const subject = Subject();
    const output = [];

    subject.subscribe(value => output.push(value));

    expect(output).deep.equal([]);

    await subject(123);
    await subject(456);

    expect(output).deep.equal([123, 456]);
```

### Subject & `hi` or `pi`
You can `listen for` any `hi` or `pi` function
```javascript
    const hi1 = hi(async (value, next) => next(value));
    const subject = Subject(hi1);

    let expected = null;
    const unsubscribe = subject.subscribe(value => expected = value);

    await hi1('okk1');

    unsubscribe()
    expect(expected).equal('okk1');
```

```javascript
```
